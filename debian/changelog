rmysql (0.11.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 08 Feb 2025 09:42:52 -0600

rmysql (0.10.29-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 04 Oct 2024 08:46:42 -0500

rmysql (0.10.28-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 29 Aug 2024 08:45:52 -0500

rmysql (0.10.27-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 09 Dec 2023 16:44:34 -0600

rmysql (0.10.26-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Oct 2023 17:26:16 -0500

rmysql (0.10.25-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 05 Dec 2022 20:32:54 -0600

rmysql (0.10.24-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 19 Oct 2022 23:02:08 -0500

rmysql (0.10.23-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 20 Dec 2021 09:19:35 -0600

rmysql (0.10.22-2) unstable; urgency=medium

  * Simple rebuild for unstable following Debian 11 release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Aug 2021 21:22:08 -0500

rmysql (0.10.22-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)
  
  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 01 Jul 2021 14:13:50 -0500

rmysql (0.10.21-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 15 Dec 2020 17:58:13 -0600

rmysql (0.10.20-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 14 Mar 2020 09:26:20 -0500

rmysql (0.10.19-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 06 Feb 2020 06:36:57 -0600

rmysql (0.10.18-2) unstable; urgency=medium

  * Source-only upload

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 19 Jan 2020 17:43:17 -0600

rmysql (0.10.18-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 19 Dec 2019 23:03:36 -0600

rmysql (0.10.17-2) unstable; urgency=medium

  * Source-only upload
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 18 Aug 2019 08:01:09 -0500

rmysql (0.10.17-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 06 Mar 2019 19:56:56 -0600

rmysql (0.10.16-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Jan 2019 11:41:37 -0600

rmysql (0.10.15-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 13 May 2018 16:23:59 -0500

rmysql (0.10.14-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 11 Mar 2018 08:49:12 -0500

rmysql (0.10.13-2) unstable; urgency=medium

  * debian/control: See Build-Depends: to default-libmysqlclient-dev
    instead of hard-wiring mariadb (closes: #890290)

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 12 Feb 2018 19:36:19 -0600

rmysql (0.10.13-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

  * debian/format: Increased to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 14 Aug 2017 16:37:37 -0500

rmysql (0.10.12-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 09 Jul 2017 11:08:37 -0500

rmysql (0.10.11-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 01 Apr 2017 12:21:03 -0500

rmysql (0.10.9-3) unstable; urgency=medium

  * debian/control: Change Build-Depends to libmariadb-dev (Closes: #843842)
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 10 Nov 2016 06:12:59 -0600

rmysql (0.10.9-2) unstable; urgency=medium

  * debian/compat: Created
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 22 Jun 2016 18:36:48 -0500

rmysql (0.10.9-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set (Build-)Depends: to (r-cran-)dbi (>= 0.4)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 09 May 2016 21:47:37 -0500

rmysql (0.10.8-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 28 Jan 2016 19:52:00 -0600

rmysql (0.10.7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 27 Oct 2015 14:31:42 -0500

rmysql (0.10.6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 05 Sep 2015 11:50:29 -0500

rmysql (0.10.5-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 30 Aug 2015 11:06:31 -0500

rmysql (0.10.4-1) unstable; urgency=low

  * New upstream release

  * debian/control: Switch Build-Depends: to libmariadb-client-lgpl-dev

  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Jul 2015 20:37:29 -0500

rmysql (0.10.3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 23 May 2015 09:15:06 -0500

rmysql (0.10.2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 24 Feb 2015 07:04:30 -0600

rmysql (0.10.1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 17 Jan 2015 17:04:15 -0600

rmysql (0.10-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 06 Jan 2015 17:43:18 -0600

rmysql (0.9-3-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  * debian/control: Set Standards-Version: to current version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 30 Mar 2013 17:59:45 -0500

rmysql (0.9-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 17 Jan 2012 16:02:21 -0600

rmysql (0.9-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 14 Jan 2012 11:13:21 -0600

rmysql (0.8-0-2) unstable; urgency=low

  * debian/control: Change Build-Depends: to libmysqlclient-dev
  							(Closes: #652137)
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 14 Dec 2011 20:38:51 -0600

rmysql (0.8-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 09 Jun 2011 17:17:49 -0500

rmysql (0.7-5-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 26 Jul 2010 06:43:18 -0500

rmysql (0.7-4-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Nov 2009 14:10:35 -0600

rmysql (0.7-4-1) unstable; urgency=low

  * New upstream release

  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.8.1

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 14 Apr 2009 15:49:12 -0500

rmysql (0.7-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 04 Feb 2009 19:02:20 -0600

rmysql (0.7-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Dec 2008 16:44:49 -0600

rmysql (0.6.1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.8.0
  
  * debian/copyright: Corrected (was verbatim copy from another package)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 08 Sep 2008 10:58:09 -0500

rmysql (0.6.0-2) unstable; urgency=low

  * debian/control: Updated (Build-)Depends: on r-cran-dbi (Closes: #439093)
  * debian/control: Upgraded (Build-)Depends: to r-base-core (>= 2.5.1)

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 22 Aug 2007 06:07:51 -0500

rmysql (0.6.0-1) unstable; urgency=low

  * New upstream release
  * debian/control: Upgraded (Build-)Depends: to r-base-core (>= 2.5.0)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 04 Jun 2007 20:28:01 -0500

rmysql (0.5.11-1) unstable; urgency=low

  * New upstream release
  * debian/control: Upgraded (Build-)Depends: to r-base-core (>= 2.4.1)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri,  5 Jan 2007 21:06:16 -0600

rmysql (0.5.10-1) unstable; urgency=low

  * New upstream release

  * debian/control: Upgraded (Build-)Depends: to r-base-core (>= 2.4.0)
    and r-cran-dbi (>= 0.1.11)
  * debian/control: Removed non-DD Steffen as an uploader
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 28 Oct 2006 11:47:26 -0500

rmysql (0.5.9-1) unstable; urgency=low

  * New maintainer, keeping Steffen as Uploaders:
  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Updated (Build-)Depends: to r-cran-dbi (>= 0.1.10-2)
    to provide DBI compiled under R 2.4.0
  * debian/control: Upgraded (Build-)Depends: to r-base-core (>> 2.3.1)
  * debian/control: Upgraded Standards-Version: to 3.7.2

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Oct 2006 08:58:11 -0500

rmysql (0.5.7-1) unstable; urgency=low

  * New upstream release
  * debian/control: Added Dirk Eddelbuettel to Uploaders:
  * debian/watch: Updated regular expression

 -- Steffen Moeller <steffen_moeller@gmx.de>  Sun, 05 Feb 2006 15:38:00 +0100

rmysql (0.5.5-3) unstable; urgency=low

  * Rebuilt with libmysqlclient-dev15 (Closes:Bug#322520).

  [ Dirk Eddelbuettel ]
  * debian/post{inst,rm}: No longer call R to update html help index

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Sat, 01 Dec 2005 15:02:20 +0100

rmysql (0.5.5-2.1) unstable; urgency=low

  * NMU
  * Coordinated with Maintainer as part of larger R 2.0.0 update effort
  * Rebuilt under R 2.0.0
  * debian/control: Updated Build-Depends: and Depends: accordingly

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  9 Oct 2004 20:44:42 -0500

rmysql (0.5.5-2) unstable; urgency=low

  * Added postinst script.
  * Added postrm script too [ Dirk Eddelbuettel ]
  * debian/control: Updated Depends to R 1.9.1 and DBI 0.1.8 [ Dirk Eddelbuettel ]
  * debian/rules: Remove config.{log,cache,status} after build [ Dirk Eddelbuettel ]
  * debian/rules: Don't use single-quotes around $(debrlib) [ Dirk Eddelbuettel ]

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Tue,  19 Sep 2004 23:33:00 +0100

rmysql (0.5.5-1) unstable; urgency=low

  * New upstream release.

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Tue,  19 Sep 2004 01:47:03 +0100

rmysql (0.5.2-2) unstable; urgency=low

  * Updated requirements for R >= 1.8.0

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Tue,  24 Nov 2003 09:02:03 +0100

rmysql (0.5.2-1) unstable; urgency=low

  * Initial Debian Release (Closes:#222319).

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Tue,  24 Nov 2003 09:02:03 +0100


